import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { RiDashboardFill, RiSettings5Fill, RiSecurePaymentFill, RiCurrencyFill, RiShieldStarFill } from 'react-icons/ri';
import Link from './components/LinkComponent/Link';
import { useState } from 'react';
import './App.css';

function App() {
  const [activeLink, setActiveLink] = useState('');

  const links = [
    {
      name: "Dashboard",
      route: "/",
      icon: <RiDashboardFill />
    },
    {
      name: "Payments",
      route: "/payments",
      icon: <RiCurrencyFill />
    },
    {
      name: "Scenarios",
      route: "/scenarios",
      icon: <RiSecurePaymentFill />
    },
    {
      name: "Settings",
      route: "/settings",
      icon: <RiSettings5Fill />
    }
  ];

  const handleLinkClick = (name) => {
    setActiveLink(name);
  }

  return (
    <Navbar collapseOnSelect expand="lg" bg="white">
        <Navbar.Brand href="/" className="brand">
          <img 
          src="https://fugu-it.com/wp-content/uploads/2020/04/FUFU-logo-1.png"
          alt=""
          className="brand-img"
          />
          <p className="brand-text">TRACKING PAYMENT.<br/>FIGHTING FRAUD.</p>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            {
              links.map(link => {
                return <Link 
                key={link.name}
                name={link.name}
                route={link.route}
                icon={link.icon}
                isActive={activeLink === link.name}
                onLinkClick={handleLinkClick} />
              })
            }
          </Nav>
          <Nav>
            <Link 
            name="Premium Plan" 
            route="/premium" 
            icon={<RiShieldStarFill className="premium" />}
            isActive={activeLink === "Premium Plan"}
            onLinkClick={handleLinkClick} />
            
           
          </Nav>
          <div className="contact-wrapper">
            <p className="contact-name"> Johnny Appleseed <br/> <span className="contact-email">johnny@apple.com</span> </p>
          </div>
        </Navbar.Collapse>
      </Navbar>
  );
}

export default App;
