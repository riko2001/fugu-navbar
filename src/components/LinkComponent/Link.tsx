import { NavLink } from 'react-router-dom';
import './Link.scss';

interface LinkProps {
    name: string,
    route: string,
    icon: JSX.Element,
    isActive: boolean,
    onLinkClick: any
}

export default function Link(props: LinkProps) {
    return (
        <div className={props.isActive ? "container-active" : "container"}>
            <div className={props.isActive ? "icon-link-wrapper-active color-active" : "icon-link-wrapper"}>
                <div className="icon">
                    {props.icon}
                </div>
                <NavLink 
                to={props.route} 
                className="link"
                onClick={() => {props.onLinkClick(props.name)}}
                >
                {props.name}
                </NavLink>
            </div>
        </div>
    )
}